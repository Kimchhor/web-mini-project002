import React from 'react'
import {Card , Button , Dropdown} from 'react-bootstrap'

const CardHead ={
    backgroundColor:'#cabfbf',
    textAlign:'center',
    padding:'5px'
}

function Cards({data}) {
    return (
        <>
            {data.map((value)=>{
            return(
                <Card>
                    <div style={CardHead}>
                        <Dropdown>
                            <Dropdown.Toggle variant="success" id="dropdown-basic">Action</Dropdown.Toggle>
                            <Dropdown.Menu style={{ background:"azure",textAlign:"center"}}>
                                <Dropdown.Item href="#/action-1">
                                    <Button variant="primary">View</Button>
                                </Dropdown.Item>
                                <Dropdown.Item href="#/action-2">
                                    <Button variant="info">Update</Button>
                                </Dropdown.Item>
                                <Dropdown.Item href="#/action-3">
                                    <Button variant="danger">Delete</Button>
                                </Dropdown.Item>
                            </Dropdown.Menu>
                        </Dropdown>
                    </div>
                    <div style={{padding:'10px'}}>
                        <h5>{value.name}</h5>
                        <h6>Job</h6>
                        <p className="ml-4">{value.job}</p>
                    </div>
                    <div style={CardHead}>
                        <p>{value.upAt}</p>
                    </div>
                </Card>
            )
            })}
        </>
    )
}

export default Cards
