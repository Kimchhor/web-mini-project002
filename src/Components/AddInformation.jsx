import React  from 'react'
import 'bootstrap/dist/css/bootstrap.min.css';
import {Container,Form,Row,Col,Button} from 'react-bootstrap'

function AddInformation() {
    return (
        <Container>
            <Row>
                <Col>
                    <Form>
                        <h1>Peronal Information</h1>
                        <Form.Group className="mb-3" controlId="formBasicEmail">
                            <Form.Label>Username</Form.Label>
                            <Form.Control type="text" placeholder="Username" />
                        </Form.Group>
                        <Form.Group className="mb-3" controlId="formBasicPassword">
                            <Form.Label>Email</Form.Label>
                            <Form.Control type="email" placeholder="Email" />
                        </Form.Group>
                        <div className="text-right">
                            <Button className="m-1" variant="primary">Submit</Button>
                            <Button variant="secondary">Cancel</Button>
                        </div>
                        
                    </Form>
                </Col>
                <Col>
                <br/>
                <br/>
                <br/>
                <Form>
                    <h3>Gender</h3>
                    {['radio'].map((type) => (
                        <div key={`inline-${type}`} className="mb-3">
                        <Form.Check
                            inline
                            label="Male"
                            name="group1"
                            type={type}
                            id={`inline-${type}-1`}
                        />
                        <Form.Check
                            inline
                            label="Female"
                            name="group1"
                            type={type}
                            id={`inline-${type}-2`}
                        />
                        </div>
                    ))}
                    <h3>Job</h3>
                </Form>
                <Form>
                    {['checkbox'].map((type) => (
                        <div key={`inline-${type}`} className="mb-3">
                        <Form.Check
                            inline
                            label="Student"
                            name="group1"
                            type={type}
                            id={`inline-${type}-1`}
                        />
                        <Form.Check
                            inline
                            label="Teacher"
                            name="group1"
                            type={type}
                            id={`inline-${type}-2`}
                        />
                        <Form.Check
                            inline
                            label="Developer"
                            name="group1"
                            type={type}
                            id={`inline-${type}-2`}
                        />
                        </div>
                    ))}
                </Form>
                </Col>
            </Row>
        </Container>
    )
}

export default AddInformation


