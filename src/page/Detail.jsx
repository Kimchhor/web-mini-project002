import React from 'react'
import {Row ,Col ,CloseButton} from 'react-bootstrap'

function Detail({data}) {
    return (
        <div>
                
                {/* <button type="button" class="btn btn-primary" data-bs-toggle="modal" data-bs-target="#exampleModal">
                    Launch demo modal
                </button> */}
            <div class="modal fade" id="exampleModal" tabindex="-1" aria-labelledby="exampleModalLabel" aria-hidden="true">
            <div class="modal-dialog">
                <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="exampleModalLabel">Information</h5>
                    <h1 type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"><CloseButton /></h1>
                </div>
                <div class="modal-body">
                    <Row>
                        <Col md={8}>
                            <h6>Seth</h6>
                            <p>piseth99@gmail.com</p>
                            <p>Male</p>
                        </Col>
                        <Col md={4}>
                            <h6>Job</h6>
                            <ul>
                                <li>Student</li>
                            </ul>
                        </Col>
                    </Row>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-primary" data-bs-dismiss="modal">Close</button>
                </div>
                </div>
            </div>
            </div> 
        </div>
    )
}

export default Detail
