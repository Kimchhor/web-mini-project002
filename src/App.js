import React , {useState} from 'react';
import AddInformation from './Components/AddInformation';
import TableData from './Components/TableData';

function App () {
  const [data, setData] = useState([
    {
      id: 1,
      name: "Seth",
      gender: 'Male',
      email:"piseth99@gmail.com",
      job:"Student",
      creAt:"27/05/2021",
      upAt:"27/05/2021",
    },
    {
      id: 2,
      name: "Pich",
      gender: 'Male',
      email:"pich99@gmail.com",
      job:"Student",
      creAt:"27/05/2021",
      upAt:"27/05/2021",
    },
  ])

    return (
      <div>
        <AddInformation/>
        <TableData data={data}/>
      </div>
    );
}

export default App;
